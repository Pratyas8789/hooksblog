# Hooks

* Hooks are the new featutres addition in React version 16.8.
* Hooks are a powerful feature of React that make it easier to write functional components with stateful behavior.
* They allow to use React features without having to write a class.
* 'this' keyword is very confusing. When I use hooks, 'this' keyword is not in used.
* It allows me to reuse stateful logic. 

## *useState*

### *When to use usestate hooks :-*
* useState is a powerful and essential tool in functional components for managing state in React applications. When I update a state, on that time useState is used.

### *Explanation:-*
* useState is function which take initial value of state property and return current value of state property and a meathod which capable of updating of state property.

* Ex:-

        import React, { useState } from 'react'

        export default function UseState() {

        const [counter, setCounter]=useState(0)
        const handleIncreament=()=>{
            setCounter(preCounter=>preCounter+1)
        }
        const handleDecreament=()=>{
            setCounter(preCounter=>preCounter-1)
        }
        const handleReset=()=>{
            setCounter(preCounter=>preCounter=0)
        }

        return (
            <div>
                {counter}
                <br />
                <button onClick={handleIncreament}  >Increament</button>
                <br />
                <button onClick={handleDecreament}  >Decreament</button>
                <br />
                <button onClick={handleReset} >Reset</  button>
            </div>
        )
        }   

### *Explanation of example:-*
* In above exapmple, we can see that we take a counter as a number and its state (initial value) is 0. SetCount is a meathod which is capable to changing the counter variable. When I click any button like increase, decrease and reset, I call a function and inside a function setState take initial value and update it by current value.

* Finally, the work of useState updates any type of value.

## *useEffect*

### *When to use useEffect hooks :-*
* useEffect is a powerful tool for managing side effects in your functional components in React. It allows you to perform complex operations like data fetching, document title updates, and event subscriptions, while keeping your code simple and maintainable.

### *Explanation:-*
* useEffect is a function which take two arguments, first is function and second is array. When we doesnot pass second argument, useEffect is execuated after every render of a component. This is a big problem because it execuated every time which is not necesssary. So, I pass second argument as a array. When we pass second argument as a array which takes a props or state, when this props or state changes then useEffect is execuated. When we pass empty array then useEffect execuated only one times.

* Ex:- 

        import React, { useEffect, useState } from 'react'

        export default function UseEffect() {
            const [name, setName]=useState('')
            const [count, setCount]=useState(0)

        useEffect(()=>{
            console.log("without second argument");
        })

        useEffect(()=>{
            console.log("with empty array");
        },[])

        useEffect(()=>{
            console.log("with second argument");
        },[count])

        return (
            <div>
                <input type="text" value={name} onChange={event=>setName(event.target.value)}  id="" />
                <button onClick={()=>setCount(preCount=>preCount+1)} >click {count} times</button>

            </div>
        )
    }

### *Explanation of example:-*
* In above example, we can see that I take three useEffect but all three have different second argument.

        First is without any second argument. When we do not take any second argument then this useEffect is execuated every render.
        
        Second is with empty array. When we take empty array then this useEffect is execuated only one time when my program is rendered. Because it value is not change.

        Third is with some props or state. First time, it is execuated and after that when props or state is changed then useEffect is execuated.    




## *useContext*

### *When to use useContext hooks :-*

*  When I need to share data between components without the need to pass props through all levels of the component tree, on that time useContext is used.

### *Explanation:-*

* useContext is a meathod we can send data from one component to other without passing a props. Let's assume that if the component were to be nested seven or eleven levels deep. On that time we shall pass data to every component as a props. By using useContext we can send data directly to required component.

* Context provides a way to pass data through the component tree without having to pass props down manually at every level. 

* ex:-

        import React, { createContext, useContext } from 'react'

        import UseContextB from './UseContextB'

        export const userContex = createContext()

        export const channelContex = createContext()

        export default function UseContextA() {

        return (
            <div>
                <userContex.Provider value={"pratyas"}>
                    <channelContex.Provider value=                 
                        {"name"}>
                        UseContextA
                        <UseContextB />
                    </channelContex.Provider>
                </userContex.Provider>

            </div>
        )
        }


        import React from 'react'
        import UseContextC from './UseContextC'

        export default function UseContextB() {

        return (
            <div>UseContextB
                <UseContextC/>
            </div>
        )
        }


        export default function UseContextC() {
        const user=useContext(userContex)
        const channel=useContext(channelContex)
        return (
            <>
            {user} - {channel}    
        )}

## *Explanation of example:-*
* In above example, there are three component UseContextA, UseContextB and UseContextC. When we pass a value UseContextA to UseContextC directly without using props. In that case,  UseContextA create a context and in UseContextC we use useContext to use this value.

* Finally, the use of useContext is that we can pass data through one component to other without any help of intermidiate component.


## *useCallback*

### *When to use useCallback hooks :-*

* useCallback hook is used to memoize functions in order to prevent unnecessary re-renders of child components.

### *Explanation:-*

* useCallback is hooks that will return a memoized version of the callback function that only changes if one of the dependencies has changed.

* It is useful when passing callbacks to optimized child components that rely one references equality to prevent unnecessary renders.

* ex:- 

        import React, { useCallback } from 'react'
        import Title from './Title'
        import Count from './Count'
        import Button from './Button'
        import { useState } from 'react'

        export default function UseCallback() {
            const[age,setage]=useState(25)
            const[salary,setSalary]=useState(50000)

            const incrementAge=useCallback(()=>{
            setage(age+1)
        },[age])
            
            const incrementSalary=useCallback(()=>{
            setSalary(salary+1000)
        },[salary])
            

        return (
            <div>
                <Title/>
                <Count text="age" count={age} />
                <Button increment={incrementAge}/>
                <Count text="salary" count={salary} />
                <Button increment={incrementSalary}/>
            </div>
        )
        }


        import React, { memo } from 'react'

        export default memo( function  Title() {
            console.log("Title");
        return (
            <div>
                <h1>Start demo</h1>
            </div>
        )
        })


        import React, { memo } from 'react'

        export default memo( function Count({text,count}) {
        console.log(text);
        return (
            <div>
            {text}----{count} 
            </div>
        )
        })


        import React, { memo } from 'react'

        export default memo( function Button({increment}) {
            console.log("button");
        return (
            <button onClick={increment} >inc</button>
        )
        })

## *Explanation of example:*

* In this above example, we have used useCallback. When we does not use useCallBack, then I click any button the whole program is re-rendered. Due to this I use useCallback hooks. It helps us, only that component is re-rendered which is used. Rest component is not re-rendered.

* When I click on sarlary button, only sarlary button is re-rendered.


## useRef

### *When to use useRef hooks :-*

* useRef hook in React when you need to create a mutable reference to a value that persists across re-renders of your component.

### *Explanation:-*

* It is used for focusing of any element.

        import React, { useRef } from 'react'
        import { useEffect } from 'react'

        export default function UseRef() {
            const inputRef=useRef(null)
            useEffect(()=>{
                inputRef.current.focus()
            },[])
        return (
            <div>        
                <input ref={inputRef} type="text" name="" id="" />
            </div>
        )
        }

* useRef is hold the reference of a dom using ref attribute. It can be also be used to store any mutable value. 

        import React, { useEffect, useState } from 'react'
        import { useRef } from 'react'

        export default function Useref2() {
            const [timer, setTimer]= useState(0)
            const intervalRef=useRef()

            useEffect(()=>{
                intervalRef.current=setInterval(()=>{
                    setTimer(preTimer=>preTimer+1)
                },1000)

                return()=>{
                    clearInterval(intervalRef.current)
                }
            })


        return (
            <div>
                Hook-timer-{timer}
                <button onClick={()=>clearInterval(intervalRef.current)} >clear hook</button>
            </div>
        )
        }

### *Explanation of example:*
* In this above two examples, we can see that in first, when we focusing any element when the program is first time rendered, on that time useRef is used.

* In second example, by using useRef we can be called a function  which is inside an another function. Without using useref, we cannot call this function.


## useReducder

### *When to use useReducer hooks :-*

* useReducer hook in React when we need more complex state management, on that time useReducer hook is used.

### *Explanation:-*

* useReducer is a hooks that is used for state management.

* It contain two parameter first parameter is reducer function and second parameter is initial state. 

* It return a pair of values. [newState, dispatch]

* Ex:-

        import React, { useReducer } from 'react'

        const initialState = 0;
        const reducer = (state, action) => {
            switch (action) {
                case "increament":
                    return state + 1
                case "decrement":
                    return state - 1
                case "reset":
                    return initialState
                default:
                    return state
            }
        }

        export default function UseReducer() {
            const [count , dispatch]= useReducer(reducer, initialState)
            return (
                <div>
                    <div> count-{count}</div>            
                    <button onClick={()=>dispatch("increament")} >increament</button>
                    <button onClick={()=>dispatch("decrement")} >decreament</button>
                    <button onClick={()=>dispatch("reset")} >reset</button>
                </div>
            )
        }

### *Explanation of example:*

* in this above example, we have three button. All buttons have different works. When we do not use use reducer, on that time I make different function for all buttons. But when we have 10 to 15 button, on that time it is very difficult to handle that time. So, for solving this problem, I use useReducder hooks. I take another example which is below.


* Ex:-

        import React, { useReducer } from 'react'

        const initialState = {
            firstCounter: 0
        }

        const reducer = (state, action) => {
            switch (action.type) {
                case "increase":
                    return { firstCounter: state.firstCounter + action.value }
                case "decrease":
                    return { firstCounter: state.firstCounter - action.value }
                case "reset":
                    return initialState
                default:
                    return state
            }
        }

        export default function UsereducerObj() {
            const [count, dispatch] = useReducer(reducer, initialState)
            return (
                <div>
                    {count.firstCounter}
                    <br />
                    <button onClick={() => dispatch({ type: "increase", value: 1 })} >increment-1</button>
                    <button onClick={() => dispatch({ type: "decrease", value: 1 })} >decrement-1</button>
                    <button onClick={() => dispatch({ type: "increase", value: 5 })} >increment-5</button>
                    <button onClick={() => dispatch({ type: "decrease", value: 5 })} >decrement-5</button>
                    <button onClick={() => dispatch({ type: "reset" })} >reset</button>
                </div>
            )
        }

### *Explanation of example:*

* In this time, we make different five function which is very difficult. But useReducder make it very easier. By using this hooks we have not to write more codes and it is easier to understand.


## useMemo

### *Explanation:-*

* useMemo is like a useCallback. It takes two parameter, first is callBack and second is dependency.

* ex:-



        import React, { useMemo, useState } from 'react'

        export default function UseMemo() {
            const [counterOne, setCounterOne]=useState(0)
            const [counterTwo, setCounterTwo]=useState(0)

        const IncrementOne=()=>{
            setCounterOne(counterOne+1)
        }
        const IncrementTwo=()=>{
            setCounterTwo(counterTwo+1)
        }

        const isEven=useMemo(()=>{
            let i=0
            while(i<700000000) i++
            return counterOne%2==0
        },[counterOne])
   
    

        return (
            <div>
                <button onClick={IncrementOne} >counter-one-{counterOne}</button>
                <span>{isEven?"Even":"Odd"}</span>
                <button onClick={IncrementTwo} >counter-two-{counterTwo}</button>
            </div>
        )
        }

       

### *Explanation of example:*

* In above example we use two button. First is counterOne and second is counterTwo and they have own function. When we not use useMemo, then they take too much time for every click. So on that case I use useMemo. It checks every time when dependency is changed then this function is called, otherwise it doesnot called.
